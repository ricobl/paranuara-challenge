FLASK_APP='paranuara/__init__.py'

run:
	. venv/bin/activate && \
		FLASK_APP='${FLASK_APP}' \
		flask run --host localhost --reload --debugger

shell:
	. venv/bin/activate && \
		FLASK_APP='${FLASK_APP}' \
		ipython

load_fixtures:
	. venv/bin/activate && \
		FLASK_APP='${FLASK_APP}' \
		flask load_fixtures

test:
	. venv/bin/activate && \
		FLASK_APP='${FLASK_APP}' \
		FLASK_TESTING=1 \
		python -m unittest discover -s tests/

install_virtualenv:
	@which virtualenv || sudo pip install virtualenv

setup: install_virtualenv
	virtualenv -p `which python2.7` venv/
	. venv/bin/activate && pip install -r dev_requirements.txt
