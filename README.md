# Paranuara Challenge

The project uses the Flask microframework with MongoEngine as a database
abstraction for using MongoDB.

It is assumed:

* MongoDB is installed and running
* python 2.7.X is available

## Installation

The installation might require superuser if `virtualenv` is not available,
but it won't use `sudo` for installing the dependencies.

Run:

```
make setup
```

This will automatically create a `virtualenv` directory on `venv/`. There's no
need to use `virtualenv-wrapper` or other solutions.

The `requirements.txt` file has production application dependencies.
The `dev_requirements.txt` file has development application dependencies.

## Running the app

Load the provided database fixtures and run the app:

```
make load_fixtures
make run
```

The app should be available on [http://localhost:5000](http://localhost:5000).

There's a simple HTML page exposing the available views.

## Using the shell

The shell helps debugging helpers and models:

```
make shell
```

Example code loading first 15 `Company` documents:

```python
from paranuara.models import Company
Company.objects.all()[:15]
```

## Running tests

Tests use a different database to avoid destroying current app data.

```
make test
```

