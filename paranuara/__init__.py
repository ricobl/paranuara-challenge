# -*- coding: utf-8 -*-

import os

from paranuara.app import app

FLASK_TESTING = os.environ.get('FLASK_TESTING', False)

# Setup a different test database when running tests
app.config['MONGODB_SETTINGS'] = {
    'DB': FLASK_TESTING and 'paranuara_test' or 'paranuara'
}

# Load app modules
import paranuara.db # noqa
import paranuara.models # noqa
import paranuara.views # noqa
import paranuara.commands # noqa
