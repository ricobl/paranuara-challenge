# -*- coding: utf-8 -*-

import click
import json

from paranuara.app import app
from paranuara.db import db
from paranuara.models import Company, People


@app.cli.command()
def load_fixtures():
    'Load database fixtures'
    click.echo('Loading companies...')

    with open('resources/companies.json') as f:
        companies = json.load(f)

    try:
        Company.drop_collection()
    except db.OperationError:
        pass

    for company_info in companies:
        company = Company(
            index=company_info['index'],
            name=company_info['company']
        )
        company.save()

    click.echo('Loading people...')
    with open('resources/people.json') as f:
        people = json.load(f)

    try:
        People.drop_collection()
    except db.OperationError:
        pass

    for person_info in people:
        People.from_fixture(person_info)
