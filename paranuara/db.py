# -*- coding: utf-8 -*-

from flask_mongoengine import MongoEngine

from paranuara.app import app

db = MongoEngine(app)
