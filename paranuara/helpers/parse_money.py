# -*- coding: utf-8 -*-

import re

CLEANUP_RE = re.compile('[^0-9\.]')


def parse_money(money_str):
    clean_money = CLEANUP_RE.sub('', money_str)
    return float(clean_money)
