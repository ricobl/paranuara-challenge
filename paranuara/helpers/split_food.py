# -*- coding: utf-8 -*-

FRUITS = set([
    'apple',
    'banana',
    'orange',
    'strawberry',
])

VEGETABLES = set([
    'beetroot',
    'carrot',
    'celery',
    'cucumber',
])


def split_food(food):
    food_set = set(food)
    return {
        'fruits': list(FRUITS.intersection(food_set)),
        'vegetables': list(VEGETABLES.intersection(food_set)),
    }
