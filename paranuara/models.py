# -*- coding: utf-8 -*-

from dateutil.parser import parse as parse_date

from paranuara.db import db
from paranuara.helpers.split_food import split_food
from paranuara.helpers.parse_money import parse_money


class Company(db.Document):
    index = db.SequenceField()
    name = db.StringField(max_length=60)

    meta = {
        'collection': 'companies',
        'indexes': [
            {
                'fields': ['index'],
                'unique': True,
            }
        ],
    }


class People(db.Document):
    index = db.SequenceField()
    username = db.StringField()
    guid = db.StringField()
    has_died = db.BooleanField()
    balance = db.FloatField()
    picture = db.URLField()
    age = db.IntField()
    eye_color = db.StringField()
    gender = db.StringField()
    company = db.ReferenceField(Company)
    email = db.EmailField()
    phone = db.StringField()
    address = db.StringField()
    about = db.StringField()
    registered = db.DateTimeField()
    tags = db.ListField(db.StringField())
    friends = db.ListField(db.ReferenceField('People'))
    greeting = db.StringField()
    fruits = db.ListField(db.StringField())
    vegetables = db.ListField(db.StringField())

    meta = {
        'collection': 'people',
        'indexes': [
            {'fields': ['index'], 'unique': True},
        ],
    }

    @classmethod
    def from_fixture(self, person_info):
        try:
            company = Company.objects.get(index=person_info.get('company_id'))
        except Company.DoesNotExist:
            company = None

        friends_indexes = [
            f.get('index') for f in person_info.get('friends', [])
        ]
        friends = People.objects.filter(index__in=friends_indexes)

        person_info = dict(
            person_info,
            id=person_info['_id'],
            username=person_info.get('name'),
            eye_color=person_info.get('eyeColor'),
            balance=parse_money(person_info.get('balance', '0')),
            registered=parse_date(person_info.get('registered')),
            company=company,
            friends=friends,
            **split_food(person_info.get('favouriteFood'))
        )

        person_info.pop('_id', None)
        person_info.pop('name', None)
        person_info.pop('eyeColor', None)
        person_info.pop('company_id', None)
        person_info.pop('favouriteFood', None)

        person = People(**person_info)
        person.save()

        return person
