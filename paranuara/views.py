# -*- coding: utf-8 -*-

from flask import jsonify

from paranuara.app import app
from paranuara.models import Company, People

EMPLOYEE_FIELDS = ['index', 'username', 'age', 'address', 'phone']
COMPANY_FIELDS = ['index', 'name']


def error_response(status_code, info):
    response = jsonify({
        'status': status_code,
        'error': info,
    })
    response.status_code = status_code
    return response


def map_fields(data, fields):
    return dict(zip(fields, data))


def serialize_queryset(queryset, fields):
    return [
        map_fields(data, fields) for data in queryset.values_list(*fields)
    ]


def serialize_item(queryset, fields):
    return map_fields(queryset.values_list(*fields).get(), fields)


@app.route('/')
def index():
    return '''
        <h1>Paranuara</h1>
        <ul>
            <li><a href="people/">People</a></li>
            <li><a href="people/1">Person #1</a></li>
            <li><a href="companies/">Companies</a></li>
            <li><a href="companies/1">Company #1</a></li>
            <li><a href="companies/1/employees/">Employees Company #1</a></li>
            <li>
                <a href="people/3/common-alive-brown-eyed-friends/4/">
                    Person #3 and #4 and their alive friends with brown eyes
                </a>
            </li>
        </ul>
    '''


@app.route('/people/')
def people():
    data = serialize_queryset(People.objects, EMPLOYEE_FIELDS)
    return jsonify(data)


@app.route('/people/<int:index>/')
def person(index):
    person_fields = ['username', 'age', 'fruits', 'vegetables']
    try:
        data = serialize_item(
            People.objects.filter(index=index), person_fields)
    except People.DoesNotExist:
        return error_response(404, 'People not found')

    return jsonify(data)


@app.route(
    '/people/<int:index_a>'
    '/common-alive-<eye_color>-eyed-friends/'
    '<int:index_b>/'
)
def common_alive_friends(index_a, eye_color, index_b):
    person_fields = ['username', 'age', 'address', 'phone', 'friends']
    friends_fields = ['index', 'username', 'has_died', 'eye_color']

    try:
        person_a = serialize_item(
            People.objects.filter(index=index_a), person_fields)
    except People.DoesNotExist:
        return error_response(404, 'Person %s not found' % index_a)

    try:
        person_b = serialize_item(
            People.objects.filter(index=index_b), person_fields)
    except People.DoesNotExist:
        return error_response(404, 'Person %s not found' % index_b)

    friends_set_a = set(person_a['friends'])
    friends_set_b = set(person_b['friends'])
    common_friends_ids = [
        friend.id for friend in friends_set_a.intersection(friends_set_b)]

    person_a.pop('friends')
    person_b.pop('friends')

    common_friends = serialize_queryset(
        People.objects.filter(id__in=common_friends_ids,
                              eye_color=eye_color, has_died=False),
        friends_fields
    )

    data = {
        'person_a': person_a,
        'person_b': person_b,
        'common_friends': common_friends,
    }

    return jsonify(data)


@app.route('/companies/')
def companies():
    data = serialize_queryset(Company.objects, COMPANY_FIELDS)
    return jsonify(data)


@app.route('/companies/<int:index>/')
def company(index):
    try:
        data = serialize_item(Company.objects.filter(
            index=index), COMPANY_FIELDS)
    except Company.DoesNotExist:
        return error_response(404, 'Company not found')

    return jsonify(data)


@app.route('/companies/<int:index>/employees')
@app.route('/companies/<int:index>/employees/')
def employees(index):
    try:
        company = Company.objects.get(index=index)
    except Company.DoesNotExist:
        return error_response(404, 'Company not found')

    data = serialize_queryset(People.objects.filter(
        company=company), EMPLOYEE_FIELDS)

    return jsonify(data)
