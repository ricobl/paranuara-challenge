# -*- coding: utf-8 -*-

from unittest import TestCase

from paranuara import app
from paranuara.db import db


class AppTestCase(TestCase):

    def run(self, *args, **kwargs):
        try:
            return super(AppTestCase, self).run(*args, **kwargs)
        except: # noqa
            raise
        finally:
            with app.app_context():
                db_name = app.config['MONGODB_SETTINGS']['DB']
                db.connection.drop_database(db_name)

    @classmethod
    def setUpClass(cls):
        cls.app = app
        cls.db = db

        app.testing = True

        cls.client = app.test_client()
