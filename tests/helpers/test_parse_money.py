# -*- coding: utf-8 -*-

from unittest import TestCase

from paranuara.helpers.parse_money import parse_money


class ParseMoneyTestCase(TestCase):

    def test_converts_money_string_to_float(self):
        self.assertEqual(
            parse_money('$2,418.59'),
            2418.59
        )
