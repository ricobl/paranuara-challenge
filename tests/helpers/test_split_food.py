# -*- coding: utf-8 -*-

from unittest import TestCase

from paranuara.helpers.split_food import split_food


FOOD = [
    'apple',
    'banana',
    'orange',
    'strawberry',
    'beetroot',
    'carrot',
    'celery',
    'cucumber',
]


class SplitFoodTestCase(TestCase):

    def test_returns_fruits(self):
        splitted_food = split_food(FOOD)
        self.assertEqual(
            sorted(splitted_food['fruits']),
            ['apple', 'banana', 'orange', 'strawberry']
        )

    def test_returns_vegetables(self):
        splitted_food = split_food(FOOD)
        self.assertEqual(
            sorted(splitted_food['vegetables']),
            ['beetroot', 'carrot', 'celery', 'cucumber']
        )

    def test_ignores_unmapped_food(self):
        splitted_food = split_food(['pizza', 'burger'])
        self.assertEqual(splitted_food['fruits'], [])
        self.assertEqual(splitted_food['vegetables'], [])
