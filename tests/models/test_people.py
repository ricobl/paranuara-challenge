# -*- coding: utf-8 -*-

from tests.base import AppTestCase

from paranuara.models import Company, People

MINIMAL_FIXTURE = {
    "_id": "595eeb9b96d80a5bc7afb106",
    "index": 0,
    "balance": "$2,418.59",
    "company_id": 11,
    "registered": "2016-07-13T12:29:07 -10:00",
    "friends": [
    ],
    "favouriteFood": [
        "beetroot",
        "strawberry",
    ]
}


class PeopleFromFixtureTestCase(AppTestCase):

    def setUp(self):
        self.company = Company.objects.create(index=11, name='Test Company')

    def tearDown(self):
        self.company.delete()

    def test_uses_original_id(self):
        person = People.from_fixture(MINIMAL_FIXTURE)
        self.assertEqual(str(person.id), '595eeb9b96d80a5bc7afb106')

    def test_parses_balance(self):
        person = People.from_fixture(MINIMAL_FIXTURE)
        self.assertEqual(person.balance, 2418.59)

    def test_links_company_reference(self):
        person = People.from_fixture(MINIMAL_FIXTURE)
        self.assertEqual(person.company, self.company)

    def test_ignores_missing_company(self):
        person_info = dict(MINIMAL_FIXTURE, company_id=333)
        person = People.from_fixture(person_info)
        self.assertEqual(person.company, None)

    def test_links_friend_references(self):
        friend_1 = People.objects.create(index=1)
        friend_2 = People.objects.create(index=2)
        person_info = dict(
            MINIMAL_FIXTURE,
            friends=[{'index': 1}, {'index': 2}]
        )
        person = People.from_fixture(person_info)

        self.assertEqual(person.friends, [friend_1, friend_2])

    def test_splits_food(self):
        person = People.from_fixture(MINIMAL_FIXTURE)
        self.assertEqual(person.fruits, ["strawberry"])
        self.assertEqual(person.vegetables, ["beetroot"])
