# -*- coding: utf-8 -*-

import json

from tests.base import AppTestCase

from paranuara.models import People


def generate_data(index, friends=None, has_died=False, eye_color='brown'):
    return dict(
        id='595eeb9b96d80a5bc7afb10%s' % index,
        index=index,
        has_died=has_died,
        eye_color=eye_color,
        friends=friends,
        username='Person #%s' % index,
        age=index * 10,
        address='Address %s' % index,
        phone='+1 %s' % index,
    )


def create_person(index, **kwargs):
    data = generate_data(index, **kwargs)
    return People.objects.create(**data)


class CommonAliveFriendsViewTestCase(AppTestCase):

    def assertPersonEqual(self, person, data):
        self.assertEqual(
            data,
            {
                'username': person.username,
                'age': person.age,
                'address': person.address,
                'phone': person.phone,
            }
        )

    def test_returns_people_data(self):
        person_a = create_person(index=1)
        person_b = create_person(index=2)

        response = self.client.get(
            '/people/1/common-alive-brown-eyed-friends/2/')

        data = json.loads(response.data)

        self.assertPersonEqual(person_a, data['person_a'])
        self.assertPersonEqual(person_b, data['person_b'])

    def test_returns_common_alive_friends_matching_eye_color(self):
        friend_a = create_person(index=5)
        friend_b = create_person(index=6)
        exclusive_friend = create_person(index=7)

        create_person(index=1, friends=[friend_a, friend_b])
        create_person(index=2, friends=[friend_a, friend_b, exclusive_friend])

        response = self.client.get(
            '/people/1/common-alive-brown-eyed-friends/2/')

        data = json.loads(response.data)

        self.assertEqual(
            data['common_friends'],
            [
                {
                    u'eye_color': u'brown',
                    u'has_died': False,
                    u'index': 5,
                    u'username': u'Person #5'
                },
                {
                    u'eye_color': u'brown',
                    u'has_died': False,
                    u'index': 6,
                    u'username': u'Person #6'
                }
            ]
        )

    def test_excludes_eye_color_mismatch(self):
        friend_a = create_person(index=5)
        friend_b = create_person(index=6, eye_color='green')

        friends = [friend_a, friend_b]
        create_person(index=1, friends=friends)
        create_person(index=2, friends=friends)

        response = self.client.get(
            '/people/1/common-alive-brown-eyed-friends/2/')

        data = json.loads(response.data)

        self.assertEqual(
            data['common_friends'],
            [
                {
                    u'eye_color': u'brown',
                    u'has_died': False,
                    u'index': 5,
                    u'username': u'Person #5'
                },
            ]
        )

    def test_excludes_diceased_friends(self):
        friend_a = create_person(index=5)
        friend_b = create_person(index=6, has_died=True)

        friends = [friend_a, friend_b]
        create_person(index=1, friends=friends)
        create_person(index=2, friends=friends)

        response = self.client.get(
            '/people/1/common-alive-brown-eyed-friends/2/')

        data = json.loads(response.data)

        self.assertEqual(
            data['common_friends'],
            [
                {
                    u'eye_color': u'brown',
                    u'has_died': False,
                    u'index': 5,
                    u'username': u'Person #5'
                },
            ]
        )
