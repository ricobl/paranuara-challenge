# -*- coding: utf-8 -*-

import json

from tests.base import AppTestCase

from paranuara.models import Company


class CompanyListViewTestCase(AppTestCase):

    def test_returns_companies(self):
        Company.objects.create(index=1, name='Hivery')
        response = self.client.get('/companies/')
        data = json.loads(response.data)
        self.assertEqual(
            data,
            [{'index': 1, 'name': 'Hivery'}]
        )


class CompanyViewTestCase(AppTestCase):

    def test_returns_404_for_missing_company(self):
        response = self.client.get('/companies/1/')
        self.assertEqual(response.status_code, 404)
