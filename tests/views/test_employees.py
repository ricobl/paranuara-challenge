# -*- coding: utf-8 -*-

import json

from tests.base import AppTestCase

from paranuara.models import Company, People


class EmployeesViewTestCase(AppTestCase):

    def assertPersonEqual(self, person, data):
        self.assertEqual(
            data,
            {
                'index': person.index,
                'username': person.username,
                'age': person.age,
                'address': person.address,
                'phone': person.phone,
            }
        )

    def test_returns_employees(self):
        company = Company.objects.create(index=1, name='Hivery')

        person_1 = People.objects.create(
            index=1,
            username='Enrico',
            age=10,
            address='123',
            phone='+1 123',
            company=company,
        )
        person_2 = People.objects.create(
            index=2,
            username='Joao',
            age=20,
            address='456',
            phone='+1 456',
            company=company,
        )

        response = self.client.get('/companies/1/employees/')
        data = json.loads(response.data)

        self.assertPersonEqual(person_1, data[0])
        self.assertPersonEqual(person_2, data[1])

    def test_returns_empty_list_for_empty_company(self):
        Company.objects.create(index=1, name='Hivery')
        response = self.client.get('/companies/1/employees/')
        data = json.loads(response.data)
        self.assertEqual(data, [])

    def test_returns_404_for_missing_company(self):
        response = self.client.get('/companies/1/employees/')
        self.assertEqual(response.status_code, 404)
