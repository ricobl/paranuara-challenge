# -*- coding: utf-8 -*-

import json

from tests.base import AppTestCase

from paranuara.models import People


class PeopleListViewTestCase(AppTestCase):

    def test_returns_people(self):
        person = People.objects.create(
            index=1,
            username='Enrico',
            age=10,
            phone='+55 123',
            address='1234',
        )
        response = self.client.get('/people/')
        data = json.loads(response.data)
        self.assertEqual(
            data,
            [{
                'index': person.index,
                'username': person.username,
                'age': person.age,
                'phone': person.phone,
                'address': person.address,
            }]
        )


class PersonViewTestCase(AppTestCase):

    def test_returns_person(self):
        person = People.objects.create(
            index=1,
            username='Enrico',
            age=10,
            fruits=['strawberry'],
            vegetables=['celery'],
        )
        response = self.client.get('/people/1/')
        data = json.loads(response.data)
        self.assertEqual(
            data,
            {
                'username': person.username,
                'age': person.age,
                'fruits': person.fruits,
                'vegetables': person.vegetables,
            }
        )
